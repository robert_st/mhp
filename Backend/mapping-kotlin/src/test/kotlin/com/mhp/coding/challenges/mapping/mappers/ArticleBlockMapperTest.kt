package com.mhp.coding.challenges.mapping.mappers

import com.mhp.coding.challenges.mapping.mappers.ArticleBlockMapper.DEFAULT_IMAGE
import com.mhp.coding.challenges.mapping.models.db.Image
import com.mhp.coding.challenges.mapping.models.db.ImageSize
import com.mhp.coding.challenges.mapping.models.db.blocks.*
import com.mhp.coding.challenges.mapping.models.dto.ImageDto
import com.mhp.coding.challenges.mapping.models.dto.blocks.GalleryBlockDto
import com.mhp.coding.challenges.mapping.models.dto.blocks.ImageBlockDto
import com.mhp.coding.challenges.mapping.models.dto.blocks.TextBlockDto
import com.mhp.coding.challenges.mapping.models.dto.blocks.VideoBlockDto
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

internal class ArticleBlockMapperTest {

    private val validImage = Image(
        id = 1,
        url = "https://images.test.com",
        imageSize = ImageSize.SMALL,
        lastModified = Date(),
        lastModifiedBy = "Usr"
    )

    private val validImageDto = ImageDto(
        id = validImage.id,
        url = validImage.url,
        imageSize = validImage.imageSize
    )


    @Test
    fun galleryBlockWithoutImagesToDto() {
        val articleBlock = GalleryBlock(emptyList(), 1)
        val expectedDto = GalleryBlockDto(emptyList(), 1);

        val dto = ArticleBlockMapper.toDto(articleBlock)

        Assertions.assertThat(dto).isEqualTo(expectedDto)
    }

    @Test
    fun galleryBlockWithImageEntriesToDto() {
        val articleBlock = GalleryBlock(listOf(null, validImage), 1)
        val expectedDto = GalleryBlockDto(listOf(validImageDto), 1);

        val dto = ArticleBlockMapper.toDto(articleBlock)

        Assertions.assertThat(dto).isEqualTo(expectedDto)
    }


    @Test
    fun imageBlockToDto() {
        val articleBlock = ImageBlock(validImage, 1)
        val expectedDto = ImageBlockDto(validImageDto, 1)

        val dto = ArticleBlockMapper.toDto(articleBlock)

        Assertions.assertThat(dto).isEqualTo(expectedDto)
    }

    @Test
    fun imageBlockNullImageToDto() {
        val articleBlock = ImageBlock(null, 1)

        val defaultImageDto = ImageDto(
            id = DEFAULT_IMAGE.id,
            url = DEFAULT_IMAGE.url,
            imageSize = DEFAULT_IMAGE.imageSize
        )
        val expectedDto = ImageBlockDto(defaultImageDto, articleBlock.sortIndex)


        val dto = ArticleBlockMapper.toDto(articleBlock)

        Assertions.assertThat(dto).isEqualTo(expectedDto)
    }

    @Test
    fun textBlockToDto() {
        val articleBlock = TextBlock("some text", 1)
        val expectedDto = TextBlockDto(articleBlock.text, articleBlock.sortIndex)

        val dto = ArticleBlockMapper.toDto(articleBlock)

        Assertions.assertThat(dto).isEqualTo(expectedDto)
    }

    @Test
    fun videoBlockToDto() {
        val articleBlock = VideoBlock("https://videos.test.com", VideoBlockType.YOUTUBE, 1)
        val expectedDto = VideoBlockDto(articleBlock.url, articleBlock.type, articleBlock.sortIndex)

        val dto = ArticleBlockMapper.toDto(articleBlock)

        Assertions.assertThat(dto).isEqualTo(expectedDto)
    }

    @Test
    fun notYetMapped() {
        class ArticleBlockNotYetMapped(
            var unmappedProperty: String,
            override val sortIndex: Int = 0,
        ) : ArticleBlock(sortIndex)

        assertThrows<IllegalArgumentException> { ArticleBlockMapper.toDto(ArticleBlockNotYetMapped("unmapped", 1)) }
    }
}
package com.mhp.coding.challenges.mapping.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto
import com.mhp.coding.challenges.mapping.services.ArticleService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito.given
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(ArticleController::class)
@ExtendWith(MockitoExtension::class)
internal class ArticleControllerTest(
    @Autowired val mockMvc: MockMvc,
) {

    @MockBean
    lateinit var articleServiceMock: ArticleService

    @Autowired
    private lateinit var objectMapper: ObjectMapper


    private val articleDto = ArticleDto(
        id = 1,
        title = "title",
        description = "description",
        author = "author",
        blocks = emptyList()
    )


    @Test
    fun list() {
        given(articleServiceMock.list()).willReturn(listOf(articleDto))

        mockMvc.perform(get("/article"))
            .andExpect(status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(articleDto.id))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value(articleDto.title))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value(articleDto.description))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].author").value(articleDto.author))
    }

    @Test
    fun details() {
        given(articleServiceMock.articleForId(1)).willReturn(articleDto)

        mockMvc.perform(get("/article/1"))
            .andExpect(status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(articleDto.id))
            .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(articleDto.title))
            .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(articleDto.description))
            .andExpect(MockMvcResultMatchers.jsonPath("$.author").value(articleDto.author))
    }

    @Test
    fun detailsArticleNotFound() {
        given(articleServiceMock.articleForId(1)).willReturn(null)

        mockMvc.perform(get("/article/1"))
            .andExpect(status().isNotFound)
    }

    @Test
    fun create() {
        given(articleServiceMock.create(articleDto)).willReturn(articleDto)

        mockMvc.perform(
            post("/article")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(articleDto))
        )
            .andExpect(status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(articleDto.id))
            .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(articleDto.title))
            .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(articleDto.description))
            .andExpect(MockMvcResultMatchers.jsonPath("$.author").value(articleDto.author))
    }
}
package com.mhp.coding.challenges.mapping.services

import com.mhp.coding.challenges.mapping.models.db.Article
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto
import com.mhp.coding.challenges.mapping.repositories.ArticleRepository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito.any
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*


@ExtendWith(MockitoExtension::class)
internal class ArticleServiceTest {

    private lateinit var classUnderTest: ArticleService

    @Mock
    private lateinit var articleRepositoryMock: ArticleRepository


    private val article = Article(
        id = 1,
        title = "title",
        description = "description",
        author = "author",
        blocks = emptySet(),
        lastModified = Date(),
        lastModifiedBy = "usr"
    )

    private val expectedDto = ArticleDto(
        id = 1,
        title = "title",
        description = "description",
        author = "author",
        blocks = emptyList()
    )


    @BeforeEach
    internal fun setUp() {
        classUnderTest = ArticleService(articleRepositoryMock)
    }

    @Test
    fun list() {
        given(articleRepositoryMock.all()).willReturn(listOf(article))

        val list = classUnderTest.list()

        Assertions.assertThat(list.elementAt(0)).isEqualTo(expectedDto)
    }


    @Test
    fun articleForId() {
        given(articleRepositoryMock.findBy(1)).willReturn(article)

        val dto = classUnderTest.articleForId(1)

        Assertions.assertThat(dto).isEqualTo(expectedDto)
    }


    @Test
    fun articleForIdNotFound() {
        given(articleRepositoryMock.findBy(1)).willReturn(null)

        val dto = classUnderTest.articleForId(1)

        Assertions.assertThat(dto).isEqualTo(null)
    }


    @Test
    fun create() {
        Mockito.doNothing().`when`(articleRepositoryMock).create(any())

        val dtoToCreate = ArticleDto(
            title = "An Article",
            blocks = emptyList(),
            author = "",
            description = "",
            id = 1
        )

        val expectedCreatedDto = ArticleDto(
            title = "An Article",
            blocks = emptyList(),
            author = "",
            description = "",
            id = 1
        )

        val dto = classUnderTest.create(dtoToCreate)

        Assertions.assertThat(dto).isEqualTo(expectedCreatedDto)
    }
}
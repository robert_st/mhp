package com.mhp.coding.challenges.mapping.mappers

import com.mhp.coding.challenges.mapping.models.db.Article
import com.mhp.coding.challenges.mapping.models.db.blocks.TextBlock
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto
import com.mhp.coding.challenges.mapping.models.dto.blocks.TextBlockDto
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import java.util.*

internal class ArticleMapperTest {

    @Test
    fun toDto() {
        val article = Article(
            id = 1,
            title = "title",
            description = "description",
            author = "author",
            blocks = emptySet(),
            lastModified = Date(),
            lastModifiedBy = "usr"
        )

        val expectedDto = ArticleDto(
            id = 1,
            title = "title",
            description = "description",
            author = "author",
            blocks = emptyList()
        )


        val dto = ArticleMapper.toDto(article)

        Assertions.assertThat(dto.id).isEqualTo(expectedDto.id)
        Assertions.assertThat(dto.title).isEqualTo(expectedDto.title)
        Assertions.assertThat(dto.description).isEqualTo(expectedDto.description)
        Assertions.assertThat(dto.author).isEqualTo(expectedDto.author)

        Assertions.assertThat(dto.blocks.size).isEqualTo(expectedDto.blocks.size)
    }


    @Test
    fun toDtoSortedBlocks() {
        val article = Article(
            id = 1,
            title = "title",
            description = "description",
            author = "author",
            blocks = setOf(TextBlock("text2", 2), TextBlock("text1", 1)),
            lastModified = Date(),
            lastModifiedBy = "usr"
        )

        val expectedDto = ArticleDto(
            id = 1,
            title = "title",
            description = "description",
            author = "author",
            blocks = listOf(TextBlockDto("text1", 1), TextBlockDto("text2", 2)),
        )


        val dto = ArticleMapper.toDto(article)

        Assertions.assertThat(dto.id).isEqualTo(expectedDto.id)
        Assertions.assertThat(dto.title).isEqualTo(expectedDto.title)
        Assertions.assertThat(dto.description).isEqualTo(expectedDto.description)
        Assertions.assertThat(dto.author).isEqualTo(expectedDto.author)
        Assertions.assertThat(dto.blocks.elementAt(0)).isEqualTo(dto.blocks.elementAt(0))
        Assertions.assertThat(dto.blocks.elementAt(1)).isEqualTo(dto.blocks.elementAt(1))

    }
}
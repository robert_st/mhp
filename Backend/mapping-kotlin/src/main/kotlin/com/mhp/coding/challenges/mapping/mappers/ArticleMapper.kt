package com.mhp.coding.challenges.mapping.mappers

import com.mhp.coding.challenges.mapping.models.db.Article
import com.mhp.coding.challenges.mapping.models.db.blocks.ArticleBlock
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto
import java.util.*


object ArticleMapper {


    /**
     * Converts internal representation of Article to DTO.
     * Note: ArticleDto.blocks will be sorted based on ArticleBlock.sortIndex (ascending)
     *
     * @param article the object to be converted to DTO
     * @return
     */
    fun toDto(article: Article): ArticleDto {
        return ArticleDto(
            id = article.id,
            title = article.title,
            description = article.description.orEmpty(),
            author = article.author.orEmpty(),

            // the collection of ArticleBlockDto in ArticleDTO is sorted after sortIndex in ArticleBlockDTO
            blocks = article.blocks.sortedBy(ArticleBlock::sortIndex).map(ArticleBlockMapper::toDto)
        )
    }

    // Not part of the challenge / Nicht Teil dieser Challenge.
    fun fromDto(articleDto: ArticleDto?): Article = Article(
        title = "An Article",
        blocks = emptySet(),
        id = 1,
        lastModified = Date()
    )
}

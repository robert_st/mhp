package com.mhp.coding.challenges.mapping.services

import com.mhp.coding.challenges.mapping.mappers.ArticleMapper
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto
import com.mhp.coding.challenges.mapping.repositories.ArticleRepository
import org.springframework.stereotype.Service

@Service
class ArticleService(
    private val articleRepository: ArticleRepository
) {

    fun list(): List<ArticleDto> {
        return articleRepository.all().map(ArticleMapper::toDto)
    }

    fun articleForId(id: Long): ArticleDto? {
        val article = articleRepository.findBy(id) ?: return null
        return ArticleMapper.toDto(article);
    }

    fun create(articleDto: ArticleDto): ArticleDto {
        val article = ArticleMapper.fromDto(articleDto)
        articleRepository.create(article)
        return ArticleMapper.toDto(article)
    }
}

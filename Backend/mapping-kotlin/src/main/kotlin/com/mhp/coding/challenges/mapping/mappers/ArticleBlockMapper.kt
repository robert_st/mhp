package com.mhp.coding.challenges.mapping.mappers

import com.mhp.coding.challenges.mapping.models.db.Image
import com.mhp.coding.challenges.mapping.models.db.ImageSize
import com.mhp.coding.challenges.mapping.models.db.blocks.*
import com.mhp.coding.challenges.mapping.models.dto.ImageDto
import com.mhp.coding.challenges.mapping.models.dto.blocks.*
import java.util.*


object ArticleBlockMapper {

    val DEFAULT_IMAGE = Image(
        id = -1,
        url = "",
        imageSize = ImageSize.SMALL,
        lastModified = Date(),
        lastModifiedBy = "default"
    )


    /**
     * Converts internal representation of ArticleBlock to DTO.
     *
     * @param articleBlock the object to be converted to DTO
     * @return DTO representation
     * @throws IllegalArgumentException if mapper not available
     */
    fun toDto(articleBlock: ArticleBlock): ArticleBlockDto {
        return when (articleBlock) {
            is GalleryBlock -> toDto(articleBlock)
            is ImageBlock -> toDto(articleBlock)
            is TextBlock -> toDto(articleBlock)
            is VideoBlock -> toDto(articleBlock)
            else -> throw IllegalArgumentException("Unknown articleBlock: " + articleBlock.javaClass.simpleName)
        }
    }

    private fun toDto(galleryBlock: GalleryBlock): GalleryBlockDto {
        return GalleryBlockDto(
            images = galleryBlock.images.filterNotNull().map(this::toDto),
            sortIndex = galleryBlock.sortIndex
        )
    }

    private fun toDto(image: Image): ImageDto {
        return ImageDto(
            id = image.id,
            url = image.url,
            imageSize = image.imageSize
        )
    }

    private fun toDto(imageBlock: ImageBlock): ImageBlockDto {
        val image: Image = imageBlock.image ?: DEFAULT_IMAGE

        return ImageBlockDto(
            image = toDto(image),
            sortIndex = imageBlock.sortIndex
        )
    }

    private fun toDto(textBlock: TextBlock): TextBlockDto {
        return TextBlockDto(
            text = textBlock.text,
            sortIndex = textBlock.sortIndex
        )
    }

    private fun toDto(videoBlock: VideoBlock): VideoBlockDto {
        return VideoBlockDto(
            url = videoBlock.url,
            type = videoBlock.type,
            sortIndex = videoBlock.sortIndex
        )
    }
}